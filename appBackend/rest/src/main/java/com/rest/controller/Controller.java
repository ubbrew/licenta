package com.rest.controller;

import com.model.entities.Book;
import com.model.entities.User;
import com.rest.security.models.AuthenticationRequest;
import com.service.exception.ServiceException;
import com.service.responses.Response;
import com.service.services.LibraryService;
import exceptions.AuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8081")
@RestController("/")
public class Controller {
    @Autowired
    private LibraryService libraryService;
    @Autowired
    private AuthenticationController authenticationController;

    //REST methods for USER
    @GetMapping("/users")
    public List<User>  findAllUsers() { return libraryService.findAllUsers();}

    @GetMapping("/users/{username}")
    public User getUserDetails(@PathVariable String username) {
        return authenticationController.getUserDetails(username); }

    @PostMapping("/register")
    public ResponseEntity<?> addUser(@RequestBody User user) {
        //validari
        libraryService.saveUser(user);
        return ResponseEntity.ok("User iregistrat cu succes!");
    }

    @PutMapping("/users/{id}")
    public Response updateUser(@PathVariable int id, @RequestBody User user) {
        user.setUserId(id);
        return libraryService.saveUser(user);
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable int id) throws ServiceException {
        User user = libraryService.findByIdUser(id);
        libraryService.deleteUser(user);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws AuthenticationException {
        return authenticationController.createAuthenticationToken(authenticationRequest);
    }

    @PostMapping("/books")
    public void addBook(@RequestBody Book book){
        libraryService.addBook(book);
    }

    @PutMapping("/books")
    public void updateBook(@RequestBody Book book){
        libraryService.updateBook(book);
    }

    @PostMapping("/books/addRead")
    public ResponseEntity<String> addBookToRead(@RequestParam String email, @RequestBody Book book) {
       return libraryService.addBookToRead(email, book);
    }

    @GetMapping("/books/read")
    public List<Book> getReadBooksForUser(@RequestParam String email) {
        return libraryService.getReadBooksForUser(email);
    }

    @GetMapping("/books/{id}")
    public Book getBookWithId(@PathVariable String id) {
        return libraryService.getBookWithId(id);
    }

//    @GetMapping("/books/reading")
//    public List<Book> getReadingBooksForUser(@RequestParam String email) {
//        return libraryService.getReadingBooks(email);
//    }

    @GetMapping("/books/read/total")
    public Integer getTotalReadBooks(@RequestParam String email) {
        return libraryService.getTotalReadBooks(email);
    }

    @GetMapping("/books/reviewed/total")
    public Integer getTotalReviewedBooks(@RequestParam String email) {
        return libraryService.getTotalReviewedBooks(email);
    }

    @GetMapping("/books/wishlist/total")
    public Integer getTotalWishListBooks(@RequestParam String email) {
        return libraryService.getTotalWishListBooks(email);
    }

    @GetMapping("/books/wishListed")
    public List<Book> getWishListedBooksForUser(@RequestParam String email) {
        return libraryService.getWishListedBooksForUser(email);
    }

    @PostMapping("/books/addToWishList")
    public ResponseEntity<String> addBookToWishList(@RequestParam String email, @RequestBody Book book) {
        return libraryService.addBookToWishList(email, book);
    }
//
//    @PostMapping("/books/addToReadingList")
//    public ResponseEntity<String> addBookToReadingList(@RequestParam String email, @RequestBody Book book) {
//        return libraryService.addBookToReadingList(email, book);
//    }
    @PostMapping("/books/reviews")
    public Book addReviewToBook(@RequestParam String email, @RequestParam String review, @RequestParam Double rating,  @RequestBody Book book) {
        return libraryService.addReviewToBook(email, review, rating, book);
    }

    @GetMapping("/books/recommendation")
    public List<Book> getRecommendationsForUser(@RequestParam String username,@RequestParam String bookId){
        return libraryService.getRecommendationsForUser(username, bookId);
    }
}
