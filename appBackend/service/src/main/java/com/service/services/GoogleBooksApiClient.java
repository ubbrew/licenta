package com.service.services;

import com.model.entities.Book;
import com.model.entities.BookListApiCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public
class GoogleBooksApiClient {

    @Autowired
    private RestTemplate restTemplate;

    public Book getBookFromGoogleApiWithId(String id) {
       return restTemplate.getForObject("https://www.googleapis.com/books/v1/volumes/" + id, Book.class);
    }

    public List<Book> getBooksFromGoogleApiFromAuthor(String author) {
        BookListApiCall books = restTemplate.getForObject("https://www.googleapis.com/books/v1/volumes?q=author:" +
                author + "&maxResults=15", BookListApiCall.class);
        return books.getItems();
    }

    public List<Book> getBookFromGoogleApiFromCategory(String category) {
        BookListApiCall books = restTemplate.getForObject("https://www.googleapis.com/books/v1/volumes?q=subject:" +
                category + "&maxResults=15", BookListApiCall.class);
        return books.getItems();
    }
}
