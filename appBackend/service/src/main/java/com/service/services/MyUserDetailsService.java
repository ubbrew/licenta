package com.service.services;

import com.model.entities.User;
import com.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findUserByEmail(username);
        if(userOptional.isPresent()) {
            User user = userOptional.get();
            return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), new ArrayList<>());
        }
        else throw new UsernameNotFoundException("Nume sau parola gresita!");
    }

    public User getUserDetails(String username) throws UsernameNotFoundException {
        Optional<User> userAppOptional = userRepository.findUserByEmail(username);
        if(userAppOptional.isPresent()) {
            return userAppOptional.get();
        }
        else throw new UsernameNotFoundException("Wrong username or password!");
    }
}
