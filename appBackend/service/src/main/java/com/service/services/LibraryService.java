package com.service.services;

import com.model.entities.Book;
import com.model.entities.Review;
import com.model.entities.User;
import com.model.repository.BookRepository;
import com.model.repository.UserRepository;
import com.service.exception.NoBookFoundException;
import com.service.exception.ServiceException;
import com.service.exception.UserNotFoundException;
import com.service.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Transactional(rollbackFor = Exception.class)
@Service
public class LibraryService {

    private UserRepository userRepository;
    private BookRepository bookRepository;
    private GoogleBooksApiClient googleBooksApiClient;
    private static final Random RANDOM = new Random();

    @Autowired
    public LibraryService(UserRepository userRepository, BookRepository bookRepository,
                          GoogleBooksApiClient googleBooksApiClient) {
        this.userRepository = userRepository;
        this.bookRepository = bookRepository;
        this.googleBooksApiClient = googleBooksApiClient;
    }

    //SERVICES for USER
    public List<User> findAllUsers() {
        return (List<User>) this.userRepository.findAll();
    }

    public User findByIdUser(int userID) throws ServiceException {

        Optional<User> optionalUser = userRepository.findById(userID);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        }
        throw new ServiceException("The searched user does not exist");
    }

    public Response saveUser(User user) {
        userRepository.save(user);
        return new Response("User " + user.getName() + " was added successfully!");
    }

    public void deleteUser(User user) {
        this.userRepository.delete(user);
    }

    public void addBook(Book book) {
        bookRepository.save(book);
    }

    public void updateBook(Book book) {
        bookRepository.save(book);
    }

    public Book findBookById(String id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            return optionalBook.get();
        }
        throw new NoBookFoundException("Cannot find the specific book!");
    }

    public ResponseEntity<String> addBookToRead(String email, Book book) {
        String unQuotedEmail = getUnquotedEmailAddress(email);
        Optional<Book> optionalBook = bookRepository.findById(book.getId());
        if (!optionalBook.isPresent()) {
            bookRepository.save(book);
        }
        User user = findUserByEmail(unQuotedEmail);

        if (userReadThisBook(user, book)) {
            return new ResponseEntity<>("This book is already on your list!", HttpStatus.FORBIDDEN);
        } else {
            user.addReadBook(book);
            userRepository.save(user);
            return new ResponseEntity<>("The book was added to your list!", HttpStatus.OK);
        }
    }

    public ResponseEntity<String> addBookToWishList(String email, Book book) {
        String unQuotedEmail = getUnquotedEmailAddress(email);
        Optional<Book> optionalBook = bookRepository.findById(book.getId());
        if (!optionalBook.isPresent()) {
            bookRepository.save(book);
        }
        User user = findUserByEmail(unQuotedEmail);

        if (userHasThisBookInWishList(user, book)) {
            return new ResponseEntity<>("This book is already on your list!", HttpStatus.FORBIDDEN);
        } else {
            user.addBookToWishList(book);
            userRepository.save(user);
            return new ResponseEntity<>("The book was added to your list!", HttpStatus.OK);
        }
    }

    private boolean userHasThisBookInWishList(User user, Book book) {
        return user
                .getWishListBooks()
                .stream()
                .map(Book::getId)
                .anyMatch(id -> id.equals(book.getId()));
    }


    private String getUnquotedEmailAddress(String email) {
        return email.substring(1, email.length() - 1);
    }

    private User findUserByEmail(String unQuotedEmail) {
        Optional<User> optionalUser = userRepository.findUserByEmail(unQuotedEmail);
        if (optionalUser.isPresent()) {
            return  optionalUser.get();
        }
        throw new UserNotFoundException("User not found! ");

    }

    private boolean userReadThisBook(User user, Book book) {
        return user
                .getReadBooks()
                .stream()
                .map(Book::getId)
                .anyMatch(id -> id.equals(book.getId()));
    }

    public List<Book> getReadBooksForUser(String email) {
        String unQuotedEmail = getUnquotedEmailAddress(email);
        User user = findUserByEmail(unQuotedEmail);
        return user.getReadBooks();
    }

    public List<Book> getWishListedBooksForUser(String email) {
        String unQuotedEmail = getUnquotedEmailAddress(email);
        User user = findUserByEmail(unQuotedEmail);
        return user.getWishListBooks();
    }

    public Integer getTotalReadBooks(String email) {
        User user = findUserByEmail(getUnquotedEmailAddress(email));
        return user.getReadBooks().size();
    }

    public Integer getTotalReviewedBooks(String email) {
        User user = findUserByEmail(getUnquotedEmailAddress(email));
        return user.getReviewedBooks().size();
    }

    public Integer getTotalWishListBooks(String email) {
        User user = findUserByEmail(getUnquotedEmailAddress(email));
        return user.getWishListBooks().size();
    }

    public Book addReviewToBook(String email, String reviewMessage, Double rating, Book book) {
        String unQuotedEmail = getUnquotedEmailAddress(email);
        User user = findUserByEmail(unQuotedEmail);
        Review review = Review
                .builder()
                .message(reviewMessage)
                .rating(rating)
                .username(user.getSurname() + " " + user.getName())
                .build();
        Book apiBook = findBookById(book.getId());
        apiBook.addReview(review);
        if(!userReviewedThisBook(user, apiBook)) {
            user.addBookToReviewed(apiBook);
            userRepository.save(user);
        }
        bookRepository.save(apiBook);
        return apiBook;
    }

    private boolean userReviewedThisBook(User user, Book book) {
        return user
                .getReviewedBooks()
                .stream()
                .map(Book::getId)
                .anyMatch(id -> id.equals(book.getId()));
    }

    public Book getBookWithId(String id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if(optionalBook.isPresent()){
            return optionalBook.get();
        }
        else {
            Book book = googleBooksApiClient.getBookFromGoogleApiWithId(id);
            bookRepository.save(book);
            return book;
        }
    }

    public List<Book> getRecommendationsForUser(String username, String bookId) {
        User user = findUserByEmail(getUnquotedEmailAddress(username));
        Book book = googleBooksApiClient.getBookFromGoogleApiWithId(bookId);
        List<User> users = userRepository.findAllOtherUsers(user.getEmail());
        int recommendationSize = 5;
        List<User> commonUsers = getCommonUsers(book, users);
        List<Book> candidateBooks = getRecommendationsFromUsers(book, commonUsers);
        List<Book> recommendationBooks = new ArrayList<>(getListOfRecommendationsFromUsers(candidateBooks));
        getRecommendationsFromTheSameAuthor(bookId, book, recommendationBooks);
        getRecommendationsFromCategory(book, recommendationSize, recommendationBooks);
        return recommendationBooks;
    }

    private void getRecommendationsFromCategory(Book book, int recommendationSize, List<Book> recommendationBooks) {
        String categoryName = book.getVolumeInfo().getCategories().get(0).getName() == null ?
                "Literary Fiction" : book.getVolumeInfo().getCategories().get(0).getName();

        List<Book> recommendations = googleBooksApiClient.getBookFromGoogleApiFromCategory(categoryName);
        if(recommendations == null) {
            recommendations = googleBooksApiClient.getBookFromGoogleApiFromCategory("Literary Fiction");
        }
        while(recommendationBooks.size() < recommendationSize) {
            int randomIndex = RANDOM.nextInt(recommendations.size());
            if(!bookListHasBook(recommendationBooks, recommendations.get(randomIndex))) {
                recommendationBooks.add(recommendations.get(randomIndex));
            }
        }
    }

    private void getRecommendationsFromTheSameAuthor(String bookId, Book book, List<Book> recommendationBooks) {
        List<Book> booksWithTheSameAuthor = googleBooksApiClient.getBooksFromGoogleApiFromAuthor(book.getVolumeInfo()
                .getAuthors().get(0).getName());
        int currentIndex = 0;
        int recommendationsFromSameAuthorSize = 0;
        while(recommendationsFromSameAuthorSize < 2 || currentIndex > booksWithTheSameAuthor.size()) {
            if(!bookListHasBook(recommendationBooks, booksWithTheSameAuthor.get(currentIndex)) && !booksWithTheSameAuthor.get(currentIndex).getId().equals(bookId)){
                recommendationBooks.add(booksWithTheSameAuthor.get(currentIndex));
                recommendationsFromSameAuthorSize++;
            }
            currentIndex++;
        }
    }

    private List<Book> getListOfRecommendationsFromUsers(List<Book> candidateBooks) {
        List<Book> recommendationsFromUsers = new ArrayList<>();
        if(candidateBooks.size()>0){
            if(candidateBooks.size()==1){
                recommendationsFromUsers.add(candidateBooks.get(0));
            }
            else {
                int firstRecommendation = RANDOM.nextInt(candidateBooks.size());
                recommendationsFromUsers.add((candidateBooks.get(firstRecommendation)));
                int secondRecommendation;
                do{
                    secondRecommendation = RANDOM.nextInt(candidateBooks.size());
                }
                while (firstRecommendation == secondRecommendation);
                recommendationsFromUsers.add((candidateBooks.get(secondRecommendation)));
            }
        }
        return recommendationsFromUsers;
    }

    private List<User> getCommonUsers(Book book, List<User> users) {
        return users
                .stream()
                .filter(candidate -> userReviewedThisBook(candidate, book) ||
                    userReadThisBook(candidate, book) || userHasThisBookInWishList(candidate, book))
                .collect(Collectors.toList());
    }

    private List<Book> getRecommendationsFromUsers(Book book, List<User> commonUsers) {
        List<Book> candidateBooks = new ArrayList<>();
        commonUsers.forEach(commonUser -> {
            commonUser.getReviewedBooks().forEach(candidateBook -> {
                if(candidateBook.getVolumeInfo().getCategories().contains(book.getVolumeInfo().getCategories().get(0))){
                    candidateBooks.add(candidateBook);
                }
            });
            commonUser.getReadBooks().forEach(candidateBook -> {
                if(candidateBook.getVolumeInfo().getCategories().contains(book.getVolumeInfo().getCategories().get(0))){
                    candidateBooks.add(candidateBook);
                }
            });
            commonUser.getWishListBooks().forEach(candidateBook -> {
                if(candidateBook.getVolumeInfo().getCategories().contains(book.getVolumeInfo().getCategories().get(0))){
                    candidateBooks.add(candidateBook);
                }
            });
        });
        return candidateBooks;
    }

    private boolean bookListHasBook(List<Book> recommendationBooks, Book book) {
       return recommendationBooks
               .stream()
               .anyMatch(recommendationBook -> recommendationBook.getId().equals(book.getId()));
    }
}
