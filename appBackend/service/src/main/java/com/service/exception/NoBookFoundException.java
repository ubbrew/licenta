package com.service.exception;

public class NoBookFoundException extends RuntimeException {

    public NoBookFoundException(String message) {
        super(message);
    }
}
