import com.model.entities.User;
import com.model.repository.UserRepository;
import com.service.services.MyUserDetailsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MyUserDetailsServiceTest {
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private MyUserDetailsService userDetailsService;

    @Test
    public void testLoadUserByUsername() {
        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .reviewedBooks(new ArrayList<>())
                .readBooks(new ArrayList<>())
                .wishListBooks(new ArrayList<>())
                .build();
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user1));
        UserDetails userDetails = userDetailsService.loadUserByUsername("user1@gmail.com");
        assertThat(userDetails.getUsername().equals("user1@gmail.com"));
        assertThat(userDetails.getPassword().equals("pass1"));
    }

    @Test
    public void testLoadUserByUsernameNoUserFound() {
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername("user1@gmail.com"));
    }

    @Test
    public void testGetUserDetails() {
        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .reviewedBooks(new ArrayList<>())
                .readBooks(new ArrayList<>())
                .wishListBooks(new ArrayList<>())
                .build();
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user1));
        User user = userDetailsService.getUserDetails("user1@gmail.com");
        assertThat(user.getEmail().equals("user1@gmail.com"));
        assertThat(user.getPassword().equals("pass1"));
    }

    @Test
    public void testGetUserDetailsNoUserFound() {
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> userDetailsService.getUserDetails("user1@gmail.com"));
    }
}
