import com.model.entities.*;
import com.model.repository.BookRepository;
import com.model.repository.UserRepository;
import com.service.exception.NoBookFoundException;
import com.service.exception.ServiceException;
import com.service.exception.UserNotFoundException;
import com.service.services.GoogleBooksApiClient;
import com.service.services.LibraryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LibraryServiceTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private BookRepository bookRepository;
    @Mock
    private GoogleBooksApiClient googleBooksApiClient;
    @InjectMocks
    @Spy
    private LibraryService libraryService;

    @Test
    public void testFindByIdUser() throws ServiceException {
        doReturn(Optional.of(new User())).when(userRepository).findById(anyInt());
        libraryService.findByIdUser(1);
    }

    @Test
    public void testFindByIdUserThrows() {
        doReturn(Optional.empty()).when(userRepository).findById(anyInt());
        assertThrows (ServiceException.class, () -> libraryService.findByIdUser(1));
    }

    @Test
    public void testSaveAndDeleteUser() {
        doReturn(new User()).when(userRepository).save(any(User.class));
        doNothing().when(userRepository).delete(any(User.class));
        assertThat(libraryService.saveUser(User.builder().name("Name").build()).getStatus().contains("added"));
        libraryService.deleteUser(new User());
    }

    @Test
    public void testFindByIdBook() {
        doReturn(Optional.of(new Book())).when(bookRepository).findById(anyString());
        libraryService.findBookById("string");
    }

    @Test
    public void testFindByIdBookThrows() {
        doReturn(Optional.empty()).when(bookRepository).findById(anyString());
        assertThrows (NoBookFoundException.class, () -> libraryService.findBookById("string"));
    }

    @Test
    public void testSaveBook() {
        doReturn(new Book()).when(bookRepository).save(any(Book.class));
        libraryService.addBook(new Book());
        libraryService.updateBook(new Book());
    }

    @Test
    public void testAddBookToRead() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();

        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .readBooks(new ArrayList<>())
                .build();
        doReturn(Optional.of(book1)).when(bookRepository).findById(anyString());
        doReturn(Optional.of(user1)).when(userRepository).findUserByEmail(anyString());
        doReturn(new User()).when(userRepository).save(any(User.class));
        assertThat(libraryService.addBookToRead("user1@gmail.com", book1).getStatusCodeValue() == 200 );
    }

    @Test
    public void testAddBookToReadBookNotInDB() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();

        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .readBooks(Collections.singletonList(book1))
                .build();
        doReturn(Optional.empty()).when(bookRepository).findById(anyString());
        doReturn(book1).when(bookRepository).save(any(Book.class));
        doReturn(Optional.of(user1)).when(userRepository).findUserByEmail(anyString());
        assertThat(libraryService.addBookToRead("user1@gmail.com", book1).getStatusCode().equals(HttpStatus.FORBIDDEN));
    }

    @Test
    public void testAddBookToReadUserNotInDB() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();
        doReturn(Optional.empty()).when(bookRepository).findById(anyString());
        doReturn(book1).when(bookRepository).save(any(Book.class));
        doReturn(Optional.empty()).when(userRepository).findUserByEmail(anyString());
        assertThrows(UserNotFoundException.class, () -> libraryService.addBookToRead("user1@gmail.com", book1));
    }


    @Test
    public void testAddBookToWishList() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();

        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .wishListBooks(new ArrayList<>())
                .build();
        doReturn(Optional.of(book1)).when(bookRepository).findById(anyString());
        doReturn(Optional.of(user1)).when(userRepository).findUserByEmail(anyString());
        doReturn(new User()).when(userRepository).save(any(User.class));
        assertThat(libraryService.addBookToWishList("user1@gmail.com", book1).getStatusCodeValue() == 200 );
    }

    @Test
    public void testAddBookToWishListBookNotInDB() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();

        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .wishListBooks(Collections.singletonList(book1))
                .build();
        doReturn(Optional.empty()).when(bookRepository).findById(anyString());
        doReturn(book1).when(bookRepository).save(any(Book.class));
        doReturn(Optional.of(user1)).when(userRepository).findUserByEmail(anyString());
        assertThat(libraryService.addBookToWishList("user1@gmail.com", book1).getStatusCode().equals(HttpStatus.FORBIDDEN));
    }

    @Test
    public void testGetReadBooksForUser() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();

        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .readBooks(Collections.singletonList(book1))
                .build();
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user1));
        List<Book> response = libraryService.getReadBooksForUser("user1@gmail.com");
        assertThat(response.size() == 1);
        assertThat(response.get(0).getId().equals("id1"));
        Integer count = libraryService.getTotalReadBooks("user1@gmail.com");
        assertThat(count == 1);

    }

    @Test
    public void testGetWishListBooksForUser() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();

        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .wishListBooks(Collections.singletonList(book1))
                .build();
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user1));
        List<Book> response = libraryService.getWishListedBooksForUser("user1@gmail.com");
        assertThat(response.size() == 1);
        assertThat(response.get(0).getId().equals("id1"));
        Integer count = libraryService.getTotalWishListBooks("user1@gmail.com");
        assertThat(count == 1);
    }

    @Test
    public void testGetReviewedBooksForUser() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();

        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .reviewedBooks(Collections.singletonList(book1))
                .build();
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user1));
        Integer count = libraryService.getTotalReviewedBooks("user1@gmail.com");
        assertThat(count == 1);
    }

    @Test
    public void testAddReviewToBook() {
        VolumeInfo volumeInfo = VolumeInfo.builder().reviews(new ArrayList<>()).build();
        Book book1 = Book
                .builder()
                .id("id1")
                .volumeInfo(volumeInfo)
                .build();

        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .reviewedBooks(Collections.singletonList(book1))
                .build();
        when(bookRepository.findById(anyString())).thenReturn(Optional.of(book1));
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user1));
        doReturn(book1).when(bookRepository).save(any(Book.class));
        Book book = libraryService.addReviewToBook("email1@gmail.com", "message", 5.00, book1);
        assertThat(book.getVolumeInfo().getReviews().size() == 1);
        assertThat(book.getVolumeInfo().getReviews().get(0).getMessage().equals("message"));
    }

    @Test
    public void testAddReviewToBookNotReviewed() {
        VolumeInfo volumeInfo = VolumeInfo.builder().reviews(new ArrayList<>()).build();
        Book book1 = Book
                .builder()
                .id("id1")
                .volumeInfo(volumeInfo)
                .build();

        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .reviewedBooks(new ArrayList<>())
                .build();
        when(bookRepository.findById(anyString())).thenReturn(Optional.of(book1));
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user1));
        doReturn(book1).when(bookRepository).save(any(Book.class));
        doReturn(user1).when(userRepository).save(any(User.class));
        libraryService.addReviewToBook("email1@gmail.com", "message", 5.00, book1);
        Book book = libraryService.addReviewToBook("email1@gmail.com", "message", 5.00, book1);
        assertThat(book.getVolumeInfo().getReviews().size() == 1);
        assertThat(book.getVolumeInfo().getReviews().get(0).getMessage().equals("message"));
    }

    @Test
    public void testFindAllUsers() {
        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .reviewedBooks(new ArrayList<>())
                .build();
        User user2 = User
                .builder()
                .userId(2)
                .email("user2@gmail.com")
                .name("Name2")
                .password("pass2")
                .surname("Surname2")
                .role("USER")
                .reviewedBooks(new ArrayList<>())
                .build();
        List<User> users = Arrays.asList(user1, user2);
        when(userRepository.findAll()).thenReturn(users);
        List<User> userList = libraryService.findAllUsers();
        assertThat(userList.size() == 2);
        assertThat(userList.get(0).getName().equals("Name1"));
        assertThat(userList.get(1).getName().equals("Name2"));
    }

    @Test
    public void testGetBookWithId() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();
        when(bookRepository.findById(anyString())).thenReturn(Optional.of(book1));
        assertThat(libraryService.getBookWithId("id1").getId().equals("id1"));
    }

    @Test
    public void testGetBookWithIdNotInDB() {
        Book book1 = Book
                .builder()
                .id("id1")
                .build();
        when(bookRepository.findById(anyString())).thenReturn(Optional.empty());
        when(googleBooksApiClient.getBookFromGoogleApiWithId(anyString())).thenReturn(book1);
        doReturn(book1).when(bookRepository).save(any(Book.class));
        assertThat(libraryService.getBookWithId("id1").getId().equals("id1"));
    }

    @Test
    public void testGetRecommendationsForUser() {
        VolumeInfo volumeInfo1 = VolumeInfo
                .builder()
                .title("Alchemist")
                .authors(Collections.singletonList(new Author("Paulo Coelho")))
                .categories(Collections.singletonList(new Category(1,"Fiction")))
                .description("test description")
                .averageRating(4.5)
                .reviews(new ArrayList<>())
                .build();

        Book book1 = Book
                .builder()
                .id("id1")
                .volumeInfo(volumeInfo1)
                .build();

        VolumeInfo volumeInfo2 = VolumeInfo
                .builder()
                .title("Brida")
                .authors(Collections.singletonList(new Author("Paulo Coelho")))
                .categories(Collections.singletonList(new Category(1,"Fiction")))
                .description("test description")
                .averageRating(4.5)
                .reviews(new ArrayList<>())
                .build();

        Book book2 = Book
                .builder()
                .id("id2")
                .volumeInfo(volumeInfo2)
                .build();

        VolumeInfo volumeInfo3 = VolumeInfo
                .builder()
                .title("Magus")
                .authors(Collections.singletonList(new Author("John Fowles")))
                .categories(Collections.singletonList(new Category(1,"Fiction")))
                .description("test description")
                .averageRating(4.5)
                .reviews(new ArrayList<>())
                .build();

        Book book3 = Book
                .builder()
                .id("id3")
                .volumeInfo(volumeInfo3)
                .build();

        VolumeInfo volumeInfo4 = VolumeInfo
                .builder()
                .title("11 minutes")
                .authors(Collections.singletonList(new Author("Paulo Coelho")))
                .categories(Collections.singletonList(new Category(1,"Fiction")))
                .description("test description")
                .averageRating(4.5)
                .reviews(new ArrayList<>())
                .build();

        Book book4 = Book
                .builder()
                .id("id4")
                .volumeInfo(volumeInfo4)
                .build();

        VolumeInfo volumeInfo5 = VolumeInfo
                .builder()
                .title("Norwegian Wood")
                .authors(Collections.singletonList(new Author("Haruki Murakami")))
                .categories(Collections.singletonList(new Category(1,"Fiction")))
                .description("test description")
                .averageRating(4.5)
                .reviews(new ArrayList<>())
                .build();

        Book book5 = Book
                .builder()
                .id("id5")
                .volumeInfo(volumeInfo5)
                .build();

        VolumeInfo volumeInfo6 = VolumeInfo
                .builder()
                .title("The Time machine")
                .authors(Collections.singletonList(new Author("H.G. Wells")))
                .categories(Collections.singletonList(new Category(1,"Fiction")))
                .description("test description")
                .averageRating(4.5)
                .reviews(new ArrayList<>())
                .build();

        Book book6 = Book
                .builder()
                .id("id6")
                .volumeInfo(volumeInfo6)
                .build();

        VolumeInfo volumeInfo7 = VolumeInfo
                .builder()
                .title("Crime and Punishment")
                .authors(Collections.singletonList(new Author("Feodor Dostoievski")))
                .categories(Collections.singletonList(new Category(1,"Fiction")))
                .description("test description")
                .averageRating(4.5)
                .reviews(new ArrayList<>())
                .build();

        Book book7 = Book
                .builder()
                .id("id7")
                .volumeInfo(volumeInfo7)
                .build();
        User user1 = User
                .builder()
                .userId(1)
                .email("user1@gmail.com")
                .name("Name1")
                .password("pass1")
                .surname("Surname1")
                .role("USER")
                .reviewedBooks(new ArrayList<>())
                .readBooks(new ArrayList<>())
                .wishListBooks(new ArrayList<>())
                .build();

        User user2 = User
                .builder()
                .userId(2)
                .email("user2@gmail.com")
                .name("Name2")
                .password("pass2")
                .surname("Surname2")
                .role("USER")
                .readBooks(Arrays.asList(book1, book2, book3))
                .reviewedBooks(Arrays.asList(book1, book2, book3))
                .wishListBooks(Collections.singletonList(book4))
                .build();
        User user3 = User
                .builder()
                .userId(3)
                .email("user3@gmail.com")
                .name("Name3")
                .password("pass3")
                .surname("Surname3")
                .role("USER")
                .readBooks(new ArrayList<>())
                .reviewedBooks(new ArrayList<>())
                .wishListBooks(Arrays.asList(book1, book2))
                .build();
        User user4 = User
                .builder()
                .userId(4)
                .email("user4@gmail.com")
                .name("Name4")
                .password("pass4")
                .surname("Surname4")
                .role("USER")
                .readBooks(Arrays.asList(book2, book3))
                .reviewedBooks(Collections.singletonList(book3))
                .wishListBooks(new ArrayList<>())
                .build();
        List<User> users = Arrays.asList(user2, user3, user4);
        doReturn(book1).when(googleBooksApiClient).getBookFromGoogleApiWithId(anyString());
        doReturn(Arrays.asList(book2, book4)).when(googleBooksApiClient).getBooksFromGoogleApiFromAuthor(anyString());
        doReturn(Arrays.asList(book4, book5, book6, book7)).when(googleBooksApiClient).getBookFromGoogleApiFromCategory(anyString());
        doReturn(Optional.of(user1)).when(userRepository).findUserByEmail(anyString());
        doReturn(users).when(userRepository).findAllOtherUsers(anyString());
    libraryService.getRecommendationsForUser("user1@gmail.com", "id1");
    }
}