package com.model.repository;


import com.model.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    @Query("SELECT u FROM User u where u.email = :email and u.password = :password")
    Optional<User> findUserByCredentials(@Param("email") String email, @Param("password") String password);

    @Query("SELECT u FROM User u where u.email = :email")
    Optional<User> findUserByEmail(@Param("email") String username);

    @Query(value = "SELECT * FROM user u WHERE u.email <> :email ", nativeQuery = true)
    List<User> findAllOtherUsers(@Param("email") String email);
}
