package com.model.repository;

import com.model.entities.VolumeInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VolumeInfoRepository extends CrudRepository<VolumeInfo, Integer> {
}
