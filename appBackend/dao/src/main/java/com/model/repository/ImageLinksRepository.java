package com.model.repository;

import com.model.entities.ImageLinks;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageLinksRepository extends CrudRepository<ImageLinks, Integer> {
}
