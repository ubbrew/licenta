package com.model.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;
    private String role;
    private String surname;
    private String name;
    private String email;
    private String password;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Book> readBooks;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Book> wishListBooks;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Book> reviewedBooks;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Book> readingBooks;

    public void addReadBook(Book book) {
        this.readBooks.add(book);
    }

    public void addBookToWishList(Book book) {
        this.wishListBooks.add(book);
    }

    public void addBookToReviewed(Book book) {
        this.reviewedBooks.add(book);
    }

    public void addBookToReading(Book book) { this.readingBooks.add(book); }

    public User(String role, String surname, String name, String email, String password, List<Book> readBooks, List<Book> wishListBooks, List<Book> reviewedBooks, List<Book> readingBooks) {
        this.role = role;
        this.surname = surname;
        this.name = name;
        this.email = email;
        this.password = password;
        this.readBooks = readBooks;
        this.wishListBooks = wishListBooks;
        this.reviewedBooks = reviewedBooks;
        this.readingBooks = readingBooks;
    }

}
