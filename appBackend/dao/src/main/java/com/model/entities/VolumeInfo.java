package com.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VolumeInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String title;
    @OneToMany(cascade=CascadeType.ALL)
    private List<Author> authors;
    @Lob
    private String description;
    @OneToMany(cascade=CascadeType.ALL)
    private List<Category> categories;
    @OneToOne(cascade=CascadeType.ALL)
    private ImageLinks imageLinks;
    private Double averageRating;
    private Integer ratingsCount;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true )
    private List<Review> reviews;

    public VolumeInfo(String title, List<Author> authors, String description, List<Category> categories,
                      ImageLinks imageLinks, Double averageRating, Integer ratingsCount, List<Review> reviews) {
        this.title = title;
        this.authors = authors;
        this.description = description;
        this.categories = categories;
        this.imageLinks = imageLinks;
        this.averageRating = averageRating;
        this.ratingsCount = ratingsCount;
        this.reviews = reviews;
    }

    public void addReview(Review review) {
        this.reviews.add(review);
        if(averageRating == null){
            averageRating = 0.0;
        }
        if(ratingsCount == null) {
            ratingsCount = 0;
        }
        setAverageRating((averageRating * ratingsCount + review.getRating()) / (ratingsCount + 1));
        setRatingsCount(getRatingsCount()+1);
    }
}
