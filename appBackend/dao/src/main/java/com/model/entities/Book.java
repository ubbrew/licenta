package com.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    @Id
    private String id;
    @JsonProperty("volumeInfo")
    @OneToOne(cascade= CascadeType.ALL)
    private VolumeInfo volumeInfo;

    public void addReview(Review review) {
        volumeInfo.addReview(review);
    }
}
